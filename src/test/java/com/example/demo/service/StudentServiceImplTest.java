package com.example.demo.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.demo.dao.StudentRepository;
import com.example.demo.model.Student;



@RunWith(MockitoJUnitRunner.Silent.class)
public class StudentServiceImplTest {

	@InjectMocks
	StudentServiceImpl studentServiceImpl;

	@Mock
	StudentRepository studentRepository;

	@Test
	public void testGetAllStudentsForPositive() {
		List<Student> students = new ArrayList<Student>();
		Student student = new Student();
		student.setStudentId(7);
		student.setStudentName("sumathi");
		student.setCourses(1, "spring", "sri", 4);
		students.add(student);
		Student student2 = new Student();
		student2.setStudentId(10);
		student2.setStudentName("rajkumar");
		student2.setCourses(2, "spring", "srikanth", 4);
		students.add(student2);
		Mockito.when(studentRepository.findAll()).thenReturn(students);
		List<Student> student1 = studentServiceImpl.getAllStudents();
		Assert.assertNotNull(student1);
		Assert.assertEquals(2, student1.size());

	}

	@Test
	public void testGetAllStudentsForNegative() {
		List<Student> students = new ArrayList<Student>();
		Student student = new Student();
		student.setStudentId(-7);
		student.setStudentName("sumathi");
		student.setCourses(1, "spring", "sri", 4);
		students.add(student);
		Student student2 = new Student();
		student2.setStudentId(-10);
		student2.setStudentName("rajkumar");
		student2.setCourses(2, "spring", "srikanth", 4);
		students.add(student2);
		Mockito.when(studentRepository.findAll()).thenReturn(students);
		List<Student> student1 = studentServiceImpl.getAllStudents();
		Assert.assertNotNull(student1);
		Assert.assertEquals(2, student1.size());

	}

	@Test
	public void testFindByIdForPositive() {
		Student student = new Student();
		student.setStudentId(7);
		student.setStudentName("sumathi");
		student.setCourses(1, "spring", "sri", 4);
		Mockito.when(studentRepository.findById((long) 7)).thenReturn(Optional.of(student));
		Student student1 = studentServiceImpl.getStudentById(7);
		Assert.assertNotNull(student1);
		Assert.assertEquals(7, student1.getStudentId());
	}

	@Test
	public void testFindByIdForNegative() {
		Student student = new Student();
		student.setStudentId(-15);
		student.setStudentName("virat");
		student.setCourses(1, "spring", "sri", 4);
		Mockito.when(studentRepository.findById((long) -15)).thenReturn(Optional.of(student));
		Student student1 = studentServiceImpl.getStudentById(-15);
		Assert.assertNotNull(student1);
		Assert.assertEquals(-15, student1.getStudentId());
	}

	@Test
	public void testFindByNameForPositive() {
		Student student = new Student();
		student.setStudentId(7);
		student.setStudentName("sumathi");
		student.setCourses(1, "spring", "sri", 4);
		Mockito.when(studentRepository.findByStudentName("sumathi")).thenReturn(student);
		Student student1 = studentServiceImpl.getStudentByName("sumathi");
		Assert.assertNotNull(student1);
		Assert.assertEquals("sumathi", student1.getStudentName());
	}

	@Test
	public void testFindByNameForNegative() {
		Student student = new Student();
		student.setStudentId(-7);
		student.setStudentName("Raja");
		student.setCourses(1, "spring MVC", "sri", 4);
		Mockito.when(studentRepository.findByStudentName("Raja")).thenReturn(student);
		Student student1 = studentServiceImpl.getStudentByName("Raja");
		Assert.assertNotNull(student1);
		Assert.assertEquals("Raja", student1.getStudentName());
	}

	@Test
	public void testCreateStudentForPositive() {
		Student student = new Student(10, "Hari");
		Mockito.when(studentRepository.save(student)).thenReturn(student);
		Assert.assertEquals(student, studentServiceImpl.createStudent(student));
	}

	@Test
	public void testCreateStudentForNegative() {
		Student student = new Student(-10, "ram");
		Mockito.when(studentRepository.save(student)).thenReturn(student);
		Assert.assertEquals(student, studentServiceImpl.createStudent(student));
	}

	@Test
	public void removeStudentForPositive() {
		new Student(10, "Hari");
		studentServiceImpl.removeStudent(10);
		verify(studentRepository, times(1)).deleteById((long) 10);
	}

	@Test
	public void removeStudentForNegative() {
		new Student(-10, "Hari");
		studentServiceImpl.removeStudent(-10);
		verify(studentRepository, times(1)).deleteById((long) -10);
	}

}
