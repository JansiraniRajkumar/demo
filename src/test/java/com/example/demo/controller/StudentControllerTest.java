package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.demo.model.Student;
import com.example.demo.service.StudentService;




@RunWith(MockitoJUnitRunner.class)
public class StudentControllerTest {

	// (expected = NullPointerException.class)

	@InjectMocks
	StudentController studentController;

	@Mock
	StudentService studentService;

	@Test
	public void testStudentByIdForPositive() {
		Student student = new Student(1, "jansi");
		Mockito.when(studentService.getStudentById(Mockito.anyLong())).thenReturn(student);
		ResponseEntity<Student> student1 = studentController.getStudent(1);
		Assert.assertNotNull(student1);
		Assert.assertEquals(HttpStatus.OK, student1.getStatusCode());

	}

	@Test
	public void testStudentByIdForNegative() {
		Student student = new Student(-1, "jansi");
		Mockito.when(studentService.getStudentById(Mockito.anyLong())).thenReturn(student);
		ResponseEntity<Student> student1 = studentController.getStudent(-1);
		Assert.assertNotNull(student1);
		Assert.assertEquals(HttpStatus.OK, student1.getStatusCode());

	}

	@Test
	public void testDeleteStudentForPositive() {
		new Student(1, "jenni");
		studentService.removeStudent(1);
		ResponseEntity<String> student1 = studentController.deleteStudent(1);
		Assert.assertNotNull(student1);
		Assert.assertEquals(HttpStatus.OK, student1.getStatusCode());

	}

	@Test
	public void testDeleteStudentForNegative() {
		new Student(-1, "jenni");
		studentService.removeStudent(-1);
		ResponseEntity<String> student1 = studentController.deleteStudent(-1);
		Assert.assertNotNull(student1);
		Assert.assertEquals(HttpStatus.OK, student1.getStatusCode());

	}

	@Test
	public void testGetAllStudentForPositive() {

		List<Student> students = new ArrayList<Student>();
		Student student = new Student(1, "jansi");
		students.add(student);
		Mockito.when(studentService.getAllStudents()).thenReturn(students);
		ResponseEntity<List<Student>> re = studentController.getAllStudents(student);
		Assert.assertNotNull(re);
		Assert.assertEquals(HttpStatus.OK, re.getStatusCode());

	}

	@Test
	public void testGetAllStudentForNegative() {

		List<Student> students = new ArrayList<Student>();
		Student student = new Student(-1, "jansi");
		students.add(student);
		Mockito.when(studentService.getAllStudents()).thenReturn(students);
		ResponseEntity<List<Student>> re = studentController.getAllStudents(student);
		Assert.assertNotNull(re);
		Assert.assertEquals(HttpStatus.OK, re.getStatusCode());

	}

	@Test
	public void testUpdateStudentForPositive() {
		Student student = new Student(1, "jansi");
		Mockito.when(studentService.updateStudent(student)).thenReturn(student);
		ResponseEntity<Student> student1 = studentController.updateStudent(student);
		Assert.assertNotNull(student1);
		Assert.assertEquals(HttpStatus.OK, student1.getStatusCode());

	}

	@Test
	public void testUpdateStudentForNegative() {
		Student student = new Student(-1, "jansi");
		Mockito.when(studentService.updateStudent(student)).thenReturn(student);
		ResponseEntity<Student> student1 = studentController.updateStudent(student);
		Assert.assertNotNull(student1);
		Assert.assertEquals(HttpStatus.OK, student1.getStatusCode());

	}

}
