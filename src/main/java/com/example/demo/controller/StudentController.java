package com.example.demo.controller;

import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Course;
import com.example.demo.model.Student;
import com.example.demo.service.StudentService;


@RestController
@RequestMapping("/students")
public class StudentController {

	@Autowired
	private StudentService studentService;

	@PostMapping(value = "/student")
	public ResponseEntity<Student> addUser(@Valid @RequestBody Student student) {
		Student student1 = studentService.createStudent(student);
		return new ResponseEntity<Student>(student1, HttpStatus.OK);

	}

	@GetMapping(value = "/student5")
	public ResponseEntity<List<Student>> getAllStudents(Student student) {
		List<Student> students = studentService.getAllStudents();
		return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
	}

	@GetMapping(value = "/student1/{id}")
	public ResponseEntity<Student> getStudent(@PathVariable("id") long id) {
		Student student = studentService.getStudentById(id);
		return new ResponseEntity<Student>(student, HttpStatus.OK);

	}

	@PutMapping(value = "/student6")
	public ResponseEntity<Student> updateStudent(@Valid @RequestBody Student student) {
		Student updated = studentService.updateStudent(student);
		return new ResponseEntity<Student>(updated, HttpStatus.OK);
	}

	@GetMapping(value = "/student2/{id}")
	public ResponseEntity<Set<Course>> myCourses(@PathVariable("id") String studentName) {
		Set<Course> course = studentService.myCourses(studentName);
		return new ResponseEntity<Set<Course>>(course, HttpStatus.OK);
	}

	@DeleteMapping(value = "/student/{id}")
	public ResponseEntity<String> deleteStudent(@PathVariable("id") long studentId) {
		studentService.removeStudent(studentId);
		return new ResponseEntity<String>("Student deleted", HttpStatus.OK);
	}

}
