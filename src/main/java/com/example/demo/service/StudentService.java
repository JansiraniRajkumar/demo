package com.example.demo.service;

import java.util.List;
import java.util.Set;

import com.example.demo.model.Course;
import com.example.demo.model.Student;


public interface StudentService {
	
	public List<Student> getAllStudents();
	public Student createStudent(Student student);
    public void removeStudent(long studentId);
    public Student getStudentById(long studentId);
    public Student getStudentByName(String studentname);
    public Set<Course> myCourses(String studentName);
    public Student updateStudent(Student student);

}
